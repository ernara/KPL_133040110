/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Modul2;

/**
 *
 * @author erna
 */
public class Widget {
       public int total; // Number of elements
    
    void add(){
        if(total < Integer.MAX_VALUE){
            total++;
        } else {
            throw new ArithmeticException("Overflow");
        }
    }
    
    void remove(){
        if (total > 0) {
            total--;
        } else {
            throw new ArithmeticException("Overflow");
        }
    }
}
